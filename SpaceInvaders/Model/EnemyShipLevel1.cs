﻿using SpaceInvaders.View.Sprites;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Creates the level one enemy ships
    /// </summary>
    /// <seealso cref="SpaceInvaders.Model.EnemyShip" />
    public class EnemyShipLevel1 : EnemyShip
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnemyShipLevel1"/> class.
        /// </summary>
        public EnemyShipLevel1() : base(1)
        {
            Sprite = new EnemyShipLevel1Sprite();
        }

        /// <summary>
        /// Animates this enemy ships.
        /// </summary>
        public override void Animate()
        {
            //throw new System.NotImplementedException();
        }
    }
}