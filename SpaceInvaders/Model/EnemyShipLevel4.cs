﻿using SpaceInvaders.View.Sprites;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Creates a level 4 enemy ship
    /// </summary>
    /// <seealso cref="SpaceInvaders.Model.EnemyShip" />
    public class EnemyShipLevel4 : EnemyShip
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="EnemyShipLevel4"/> class.
        /// </summary>
        public EnemyShipLevel4() : base(4)
        {
            Sprite = new EnemyShipLevel4Sprite();
        }

        /// <summary>
        ///     Animates this level four enemy ships.
        ///     Precondition: None
        ///     Postcondition: Color of the level four enemy ships changes
        /// </summary>
        public override void Animate()
        {
            ((EnemyShipLevel4Sprite)Sprite).ChangeSpriteColor();
        }
    }
}