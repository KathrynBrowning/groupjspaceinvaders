﻿using SpaceInvaders.View.Sprites;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Creates a level three enemy ship
    /// </summary>
    /// <seealso cref="SpaceInvaders.Model.EnemyShip" />
    public class EnemyShipLevel3 : EnemyShip
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="EnemyShipLevel3"/> class.
        /// </summary>
        public EnemyShipLevel3() : base(3)
        {
            Sprite = new EnemyShipLevel3Sprite();
        }

        /// <summary>
        ///     Animates the level 3 enemy ships.
        ///     Precondition: None
        ///     Postcondition: The laser colors change.
        /// </summary>
        public override void Animate()
        {
            ((EnemyShipLevel3Sprite)Sprite).ChangeLaserColor();
        }
    }
}