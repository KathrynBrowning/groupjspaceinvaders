﻿using SpaceInvaders.View.Sprites;
using System;
using Windows.UI.Xaml;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Manages the player ship.
    /// </summary>
    /// <seealso cref="SpaceInvaders.Model.GameObject" />
    public class PlayerShip : GameObject
    {
        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether [power up].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [power up]; otherwise, <c>false</c>.
        /// </value>
        public bool PowerUp { get; set; }

        #endregion Properties

        #region Data members

        private const int SpeedXDirection = 3;
        private const int SpeedYDirection = 0;
        private const int FifteenSeconds = 15;

        private readonly DispatcherTimer timer;

        private int powerUpCounter;

        #endregion Data members

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="PlayerShip" /> class.
        /// </summary>
        public PlayerShip()
        {
            Sprite = new PlayerShipSprite();
            SetSpeed(SpeedXDirection, SpeedYDirection);
            this.timer = new DispatcherTimer();
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        ///     Determines whether the power up is active
        ///     Precondition: None
        ///     Postcondition: None
        /// </summary>
        /// <returns>
        ///   <c>true</c> if the power up is active <c>false</c>.
        /// </returns>
        public bool IsPowerUpActive()
        {
            return this.PowerUp;
        }

        /// <summary>
        ///     Initiates the enemy ship's bullet timer.
        /// </summary>
        public void InitiateTimer()
        {
            this.powerUpCounter = 0;
            TimeSpan tickInterval = new TimeSpan(0, 0, 0, 1, 0);
            this.timer.Interval = tickInterval;
            this.timer.Tick += this.timerOnTick;
            this.timer.Start();
        }

        private void timerOnTick(object sender, object o)
        {
            this.PowerUp = true;

            this.powerUpCounter++;

            if (this.powerUpCounter >= FifteenSeconds)
            {
                this.timer.Stop();
                this.PowerUp = false;
            }
        }

        #endregion Methods
    }
}