﻿using SpaceInvaders.View.Sprites;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Creates a bonus ship
    /// </summary>
    /// <seealso cref="SpaceInvaders.Model.EnemyShip" />
    public class BonusShip : EnemyShip
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="BonusShip"/> class.
        /// </summary>
        public BonusShip() : base(5)
        {
            Sprite = new BonusShipSprite();
        }

        /// <summary>
        ///     Animates this enemy ships.
        ///     Precondition: None
        ///     Postcondition: Ship's color changes
        /// </summary>
        public override void Animate()
        {
            ((BonusShipSprite)Sprite).ChangeChasisColor();
        }
    }
}