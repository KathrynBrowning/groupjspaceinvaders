﻿using SpaceInvaders.View.Sprites;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Creates a level two enemy ship.
    /// </summary>
    /// <seealso cref="SpaceInvaders.Model.EnemyShip" />
    public class EnemyShipLevel2 : EnemyShip
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="EnemyShipLevel2"/> class.
        /// </summary>
        public EnemyShipLevel2() : base(2)
        {
            Sprite = new EnemyShipLevel2Sprite();
        }

        /// <summary>
        ///     Animates this enemy ships.
        ///     Precondition: None
        ///     Postcondition: Enemy's eyes change colors
        /// </summary>
        public override void Animate()
        {
            ((EnemyShipLevel2Sprite)Sprite).ChangeEyeColor();
        }
    }
}