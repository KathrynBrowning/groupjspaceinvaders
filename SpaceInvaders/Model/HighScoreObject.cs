﻿namespace SpaceInvaders.Model
{
    /// <summary>
    /// A high score object with a name, score and level
    /// </summary>
    public class HighScoreObject
    {
        #region Properties

        /// <summary>
        /// Gets or sets the name of the player.
        /// </summary>
        /// <value>
        /// The name of the player.
        /// </value>
        public string PlayerName { get; set; }

        /// <summary>
        /// Gets or sets the player score.
        /// </summary>
        /// <value>
        /// The player score.
        /// </value>
        public int PlayerScore { get; set; }

        /// <summary>
        /// Gets or sets the player level.
        /// </summary>
        /// <value>
        /// The player level.
        /// </value>
        public int PlayerLevel { get; set; }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="HighScoreObject"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="score">The score.</param>
        /// <param name="level">The level.</param>
        public HighScoreObject(string name, int score, int level)
        {
            this.PlayerName = name;
            this.PlayerScore = score;
            this.PlayerLevel = level;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return this.PlayerName + "," + this.PlayerScore + "," + this.PlayerLevel;
        }

        #endregion Constructors
    }
}