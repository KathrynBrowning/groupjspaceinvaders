﻿namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Base EnemyShip class
    /// </summary>
    /// <seealso cref="SpaceInvaders.Model.GameObject" />
    public abstract class EnemyShip : GameObject
    {
        private readonly int shipPoints;

        /// <summary>
        ///     Initializes a new instance of the <see cref="EnemyShip"/> class.
        /// </summary>
        /// <param name="shipPoints">The ship points.</param>
        protected EnemyShip(int shipPoints)
        {
            this.shipPoints = shipPoints;
        }

        /// <summary>
        ///     "Kills" the ship by changing the opacity to zero.
        ///     Precondition: None
        ///     Postcondition: Sprite opacity = 0.
        /// </summary>
        public void KillThisShip()
        {
            Sprite.Opacity = 0;
        }

        /// <summary>
        /// Places the enemy ship.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        public void PlaceEnemyShip(double x, double y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        ///     Determines whether the enemy ship is "alive".
        ///     Precondition: None
        ///     Postcondition: None
        /// </summary>
        /// <returns>
        ///   <c>true</c> if the enemy ship is alive (opacity >= 1), otherwise <c>false</c>.
        /// </returns>
        public bool IsThisShipAlive()
        {
            return Sprite.Opacity >= 1.0;
        }

        /// <summary>
        ///     Gets a value indicating whether this instance is alive.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is alive; otherwise, <c>false</c>.
        /// </value>
        public bool IsAlive => Sprite.Opacity > 0;

        /// <summary>
        ///     Gets the ship points of the specified ship type.
        ///     Precondition: None
        ///     Postcondition: None
        /// </summary>
        /// <returns>The ship points, which differ depending on the ship.</returns>
        public int GetShipPoints()
        {
            return this.shipPoints;
        }

        /// <summary>
        ///     Animates this enemy ships.
        /// </summary>
        public abstract void Animate();
    }
}