﻿using SpaceInvaders.View;
using System;
using System.Collections.Generic;
using System.IO;
using Windows.Storage;
using Windows.Storage.Streams;
using SpaceInvaders.Managers;

namespace SpaceInvaders.Model.Managers
{
    /// <summary>
    /// Manages the High Score Objects
    /// </summary>
    public class HighScoreManager
    {
        private readonly List<HighScoreObject> highScoreObjects;
        private readonly HighScoreObject highScoreObject;

        /// <summary>
        /// Initializes a new instance of the <see cref="HighScoreManager"/> class.
        /// </summary>
        public HighScoreManager(ScoreManager screManager)
        {
            this.highScoreObjects = new List<HighScoreObject>();
            var scoreManager = screManager;
            var scoreBoard = new HighScoreBoard();

            this.highScoreObject = new HighScoreObject(scoreBoard.UserName, scoreManager.TotalPoints, scoreManager.GameLevel);
            this.highScoreObjects.Add(this.highScoreObject);
            this.readFromFile();
            this.writeToFile();
        }

        private async void readFromFile()
        {
            var storageFolder =
                ApplicationData.Current.LocalFolder;
            try
            {
                var file = await storageFolder.GetFileAsync("HighScores.txt");
                var buffer = await FileIO.ReadBufferAsync(file);
                string highScoreText;
                using (var dataReader = DataReader.FromBuffer(buffer))
                {
                    highScoreText = dataReader.ReadString(buffer.Length);
                }

                var highScore = highScoreText.Split('\n');

                foreach (string line in highScore)
                {
                    var lines = line.Split(',');
                    var playerName = lines[0];

                    if (!playerName.Equals(""))
                    {
                        var score = Int32.Parse(lines[1]);

                        var level = Int32.Parse(lines[2]);
                        this.highScoreObjects.Add(new HighScoreObject(playerName, score, level));
                    }
                }
            }
            catch (FileNotFoundException)
            {
                this.writeToFile();
            }
        }

        private async void writeToFile()
        {
            var storageFolder =
                ApplicationData.Current.LocalFolder;
            var sampleFile =
                await storageFolder.CreateFileAsync("HighScores.txt",
                    CreationCollisionOption.ReplaceExisting);

            string[] output = {
                 this.highScoreObject + Environment.NewLine
            };

            var stream = await sampleFile.OpenAsync(FileAccessMode.ReadWrite);

            using (var outputStream = stream.GetOutputStreamAt(0))
            {
                using (var dataWriter = new DataWriter(outputStream))
                {
                    foreach (var line in output)
                    {
                        dataWriter.WriteString(line);
                    }

                    await dataWriter.StoreAsync();
                    await outputStream.FlushAsync();
                }
            }
            stream.Dispose();
        }
    }
}