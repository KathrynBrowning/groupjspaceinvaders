﻿using System;
using System.IO;
using Windows.UI.Xaml;

// The Content Dialog item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SpaceInvaders.View
{
    /// <summary>
    /// Creates a high score board screen
    /// </summary>
    /// <seealso cref="Windows.UI.Xaml.Controls.ContentDialog" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector2" />
    public sealed partial class HighScoreBoard
    {
        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string UserName
        {
            get { return this.usernameBox.Text; }
            set { this.usernameBox.Text = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HighScoreBoard"/> class.
        /// </summary>
        public HighScoreBoard()
        {
            this.InitializeComponent();
        }

        private void scoreNameLevelOnChecked(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void nameLevelScoreOnChecked(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void levelNameScoreOnChecked(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void submitButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.UserName = this.usernameBox.Text;

            this.bodyText.Text = File.ReadAllText("HighScores.txt");
        }
    }
}