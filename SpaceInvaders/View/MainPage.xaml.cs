﻿using SpaceInvaders.Utilities;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using SpaceInvaders.Managers;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace SpaceInvaders.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage
    {
        /// <summary>
        ///     The application height
        /// </summary>
        public const double ApplicationHeight = 600;

        /// <summary>
        ///     The application width
        /// </summary>
        public const double ApplicationWidth = 670;

        private readonly GameManager gameManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainPage"/> class.
        /// </summary>
        public MainPage()
        {
            this.InitializeComponent();

            ApplicationView.PreferredLaunchViewSize = new Size { Width = ApplicationWidth, Height = ApplicationHeight };
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.PreferredLaunchViewSize;
            ApplicationView.GetForCurrentView().SetPreferredMinSize(new Size(ApplicationWidth, ApplicationHeight));

            this.gameManager = new GameManager(ApplicationHeight, ApplicationWidth);
            this.gameManager.InitializeGame(this.theCanvas);

            Window.Current.CoreWindow.KeyDown += this.CoreWindow_KeyDown;

            ThreadManager.RunBackgroundTask(this.keyStateMovement);
            ThreadManager.RunBackgroundTask(this.keyStateBullets);
        }

        private void CoreWindow_KeyDown(CoreWindow sender, KeyEventArgs args)
        {
            this.cheatCodes(args.VirtualKey);
        }

        private void keyStateBullets()
        {
            const int delayKeyStates = 150;
            while (true)
            {
                Task.Delay(delayKeyStates).Wait();
                ThreadManager.RunOnUiThread(this.keyStateBulletsHandler);
            }
            // ReSharper disable once FunctionNeverReturns
        }

        private void keyStateBulletsHandler()
        {
            var spaceKey = Window.Current.CoreWindow.GetKeyState(VirtualKey.Space);

            if (spaceKey.HasFlag(CoreVirtualKeyStates.Down))
            {
                this.gameManager.FirePlayerBullet();
            }
        }

        private void keyStateMovement()
        {
            const int delayKeyStates = 50;
            while (true)
            {
                Task.Delay(delayKeyStates).Wait();
                ThreadManager.RunOnUiThread(this.keyStateMovementHandler).Wait();
            }
            // ReSharper disable once FunctionNeverReturns
        }

        private void keyStateMovementHandler()
        {
            var leftKey = Window.Current.CoreWindow.GetKeyState(VirtualKey.Left);
            var rightKey = Window.Current.CoreWindow.GetKeyState(VirtualKey.Right);

            if (leftKey.HasFlag(CoreVirtualKeyStates.Down))
            {
                this.gameManager.MovePlayerShipLeft();
            }
            else if (rightKey.HasFlag(CoreVirtualKeyStates.Down))
            {
                this.gameManager.MovePlayerShipRight();
            }

            this.cheatCodesContinuous();
        }

        private void cheatCodes(VirtualKey virtualKey)
        {
            switch (virtualKey)
            {
                case VirtualKey.NumberPad9:
                    this.gameManager.KillAllEnemyShips();
                    break;

                case VirtualKey.NumberPad8:
                    this.gameManager.KillSomeEnemyShips();
                    break;
            }
        }

        private void cheatCodesContinuous()
        {
            var shiftKey = Window.Current.CoreWindow.GetKeyState(VirtualKey.Shift);
            if (shiftKey.HasFlag(CoreVirtualKeyStates.Down))
            {
                this.gameManager.Tick();
            }
        }

        private void MainPage_OnLoaded(object sender, RoutedEventArgs e)
        {
            var center = (this.theCanvas.Width / 2.0) - (this.shipsGrid.ActualWidth / 2.0);
            Canvas.SetLeft(this.shipsGrid, center);
        }
    }
}