﻿// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace SpaceInvaders.View.Sprites
{
    /// <summary>
    ///     Creates a new Player Life sprite
    /// </summary>
    /// <seealso cref="Windows.UI.Xaml.Controls.UserControl" />
    /// <seealso cref="SpaceInvaders.View.Sprites.ISpriteRenderer" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector2" />
    public sealed partial class PlayerLifeSprite
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="PlayerLifeSprite"/> class.
        ///     Precondition: none
        ///     Postcondition: Player Life sprite has been created
        /// </summary>
        public PlayerLifeSprite()
        {
            this.InitializeComponent();
        }
    }
}