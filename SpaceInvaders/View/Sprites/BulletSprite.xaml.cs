﻿// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace SpaceInvaders.View.Sprites
{
    /// <summary>
    ///     Creates a bullet sprite.
    /// </summary>
    public sealed partial class BulletSprite
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="BulletSprite" /> class.
        ///     Precondition: None
        ///     Postcondition: Bullet sprite has been initialized
        /// </summary>
        public BulletSprite()
        {
            this.InitializeComponent();
        }
    }
}