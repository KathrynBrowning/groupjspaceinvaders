﻿using Windows.UI;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace SpaceInvaders.View.Sprites
{
    /// <summary>
    ///     Creates a bonus ship sprite.
    /// </summary>
    /// <seealso cref="Windows.UI.Xaml.Controls.UserControl" />
    /// <seealso cref="SpaceInvaders.View.Sprites.ISpriteRenderer" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector2" />
    public sealed partial class BonusShipSprite
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="BonusShipSprite"/> class.
        /// </summary>
        public BonusShipSprite()
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Changes the outer chasis color of the ship
        ///     Precondition: SolidColorBrush != null
        ///     Postcondition: Sprite's color has been changed
        /// </summary>
        public void ChangeChasisColor()
        {
            var brush = this.outerShipColor.Fill as SolidColorBrush;
            if (brush == null)
            {
                return;
            }
            if (brush.Color == Colors.Black)
            {
                this.outerShipColor.Fill = new SolidColorBrush(Colors.AntiqueWhite);
            }
            else
            {
                this.outerShipColor.Fill = new SolidColorBrush(Colors.Black);
            }
        }
    }
}