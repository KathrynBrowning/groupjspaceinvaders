﻿using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace SpaceInvaders.View.Sprites
{
    /// <summary>
    /// Creates the Base Sprite
    /// </summary>
    /// <seealso cref="Windows.UI.Xaml.Controls.UserControl" />
    public abstract partial class BaseSprite : ISpriteRenderer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BaseSprite"/> class.
        /// </summary>
        protected BaseSprite()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Renders the sprite at the specified location.
        ///     Precondition: None
        ///     Postcondition: Sprite has been rendered at specified coordinates
        /// </summary>
        /// <param name="x">The x location</param>
        /// <param name="y">The y location</param>
        public void RenderAt(double x, double y)
        {
            Canvas.SetLeft(this, x);
            Canvas.SetTop(this, y);
        }
    }
}