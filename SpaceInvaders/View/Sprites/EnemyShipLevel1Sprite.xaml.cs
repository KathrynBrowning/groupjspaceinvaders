﻿using Windows.UI;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace SpaceInvaders.View.Sprites
{
    /// <summary>
    ///     Creates an EnemyShipLevel1Sprite that inherits rendering capabilities.
    /// </summary>
    /// <seealso cref="Windows.UI.Xaml.Controls.UserControl" />
    /// <seealso cref="SpaceInvaders.View.Sprites.ISpriteRenderer" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector2" />
    public sealed partial class EnemyShipLevel1Sprite
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="EnemyShipLevel1Sprite"/> class.
        ///     Precondition: None
        ///     Postcondition: Class is initialized.s
        /// </summary>
        public EnemyShipLevel1Sprite()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Changes the color of the sprite's eyes.
        ///     Precondition: SolidColorBrush != null
        ///     Postcondition: Sprite's color has been changed
        /// </summary>
        public void ChangeEyeColor()
        {
            var brush = this.leftEye.Fill as SolidColorBrush;
            if (brush == null)
            {
                return;
            }

            if (brush.Color == Colors.Black)
            {
                this.leftEye.Fill = new SolidColorBrush(Colors.Red);
                this.rightEye.Fill = new SolidColorBrush(Colors.Red);
            }
            else
            {
                this.leftEye.Fill = new SolidColorBrush(Colors.Black);
                this.rightEye.Fill = new SolidColorBrush(Colors.Black);
            }
        }
    }
}