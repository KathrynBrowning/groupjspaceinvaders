﻿// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace SpaceInvaders.View.Sprites
{
    /// <summary>
    ///     Creates the enemy ship level two sprite.
    /// </summary>
    /// <seealso cref="Windows.UI.Xaml.Controls.UserControl" />
    /// <seealso cref="SpaceInvaders.View.Sprites.ISpriteRenderer" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector2" />
    public sealed partial class EnemyShipLevel2Sprite
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="EnemyShipLevel2Sprite"/> class.
        ///     Precondition: None
        ///     Postcondition: Level two enemy ships have been initialized.
        /// </summary>
        public EnemyShipLevel2Sprite()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Changes the color of the sprite's eyes.
        ///     Precondition: SolidColorBrush != null
        ///     Postcondition: Sprite's color has been changed
        /// </summary>
        public void ChangeEyeColor()
        {
            this.mainHead.ChangeEyeColor();
        }
    }
}