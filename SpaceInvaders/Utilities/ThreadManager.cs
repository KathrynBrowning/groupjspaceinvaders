﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace SpaceInvaders.Utilities
{
    /// <summary>
    ///     Class controls the handling of all threading.
    /// </summary>
    public class ThreadManager
    {
        private static readonly object DelayGateWay = new object();
        private static readonly object BackgroundGateWay = new object();

        /// <summary>
        ///     The UI task scheduler
        /// </summary>
        public static readonly TaskScheduler UiTaskScheduler = TaskScheduler.FromCurrentSynchronizationContext();

        /// <summary>
        ///     Delays the task and allows for background threads to finish in order.
        ///     Precondition: None
        ///     Postcondition: Background threads are completed in order with a delay.
        /// </summary>
        /// <param name="delayedAction">The delayed action.</param>
        /// <param name="milliseconds">The milliseconds to delay the action.</param>
        public static Task DelayTaskWithWait(Action delayedAction, int milliseconds)
        {
            return Task.Factory.StartNew(() =>
            {
                lock (DelayGateWay)
                {
                    delayTask(delayedAction, milliseconds);
                }
            }, CancellationToken.None, TaskCreationOptions.None, TaskScheduler.Default);
        }

        /// <summary>
        ///     Delays the task.
        ///     Precondition: None
        ///     Postcondition: Task is delayed.
        /// </summary>
        /// <param name="delayedAction">The delayed action.</param>
        /// <param name="milliseconds">The milliseconds to delay the action.</param>
        public static Task DelayTask(Action delayedAction, int milliseconds)
        {
            return Task.Factory.StartNew(() =>
            {
                delayTask(delayedAction, milliseconds);
            }, CancellationToken.None, TaskCreationOptions.None, TaskScheduler.Default);
        }

        /// <summary>
        ///     Runs the background task.
        ///     Precondition: None
        ///     Postcondition: Task is run on a background thread.
        /// </summary>
        /// <param name="action">The action.</param>
        public static Task RunBackgroundTask(Action action)
        {
            return Task.Factory.StartNew(action, CancellationToken.None, TaskCreationOptions.None, TaskScheduler.Default);
        }

        /// <summary>
        ///     Runs the task on user interface thread.
        ///     Precondition: None
        ///     Postcondition: Task runs, in order, in the user interface thread
        /// </summary>
        /// <param name="action">The action.</param>
        public static Task RunOnUiThread(Action action)
        {
            return Task.Factory.StartNew(() =>
            {
                lock (BackgroundGateWay)
                {
                    action();
                }
            }, CancellationToken.None, TaskCreationOptions.None, UiTaskScheduler);
        }

        private static void delayTask(Action delayedAction, int milliseconds)
        {
            Task.Delay(milliseconds).Wait();
            Task.Factory.StartNew(delayedAction, CancellationToken.None, TaskCreationOptions.None, UiTaskScheduler);
        }
    }
}