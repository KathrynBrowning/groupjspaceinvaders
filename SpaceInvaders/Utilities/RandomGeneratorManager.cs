﻿using System;

namespace SpaceInvaders.Utilities
{
    /// <summary>
    ///     Handles the random number generation.
    /// </summary>
    public static class RandomGeneratorManager
    {
        private static readonly Random Generator;

        static RandomGeneratorManager()
        {
            Generator = new Random();
        }

        /// <summary>
        ///     Chooses a random number up to the specified max - 1
        ///     Precondition: None
        ///     Postcondition: None
        /// </summary>
        /// <param name="max">The maximum.</param>
        /// <returns>A number generated randomly between 0 and the (max - 1)</returns>
        public static int Next(int max)
        {
            return Generator.Next(max);
        }

        /// <summary>
        ///     Chooses a random number between the min and (max - 1)
        ///     Precondition: None
        ///     Postcondition: None
        /// </summary>
        /// <param name="min">The minimum.</param>
        /// <param name="max">The maximum.</param>
        /// <returns>A number generated randomly between the min and (max - 1)</returns>
        public static int Next(int min, int max)
        {
            return Generator.Next(min, max);
        }
    }
}