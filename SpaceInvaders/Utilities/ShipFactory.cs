﻿using SpaceInvaders.Model;
using System;

namespace SpaceInvaders.Utilities
{
    /// <summary>
    ///     Handles the creation of all ships.
    /// </summary>
    public class ShipFactory
    {
        /// <summary>
        ///     Creates the level one enemy ship.
        ///     Precondition: None
        ///     Postcondition: Enemy ship level one is created
        /// </summary>
        /// <returns>A new instance of a level one enemy ship</returns>
        public static EnemyShipLevel1 CreateEnemyShipLevelOne()
        {
            var enemyShipLvl1 = new EnemyShipLevel1();
            return enemyShipLvl1;
        }

        /// <summary>
        ///     Creates the level two enemy ship.
        ///     Precondition: None
        ///     Postcondition: Enemy ship level two is created
        /// </summary>
        /// <returns>A new instance of a level two enemy ship</returns>
        public static EnemyShipLevel2 CreateEnemyShipLevelTwo()
        {
            var enemyShipLvl2 = new EnemyShipLevel2();
            return enemyShipLvl2;
        }

        /// <summary>
        ///     Creates the level three enemy ship.
        ///     Precondition: None
        ///     Postcondition: Enemy ship level three is created
        /// </summary>
        /// <returns>A new instance of a level three enemy ship</returns>
        public static EnemyShipLevel3 CreateEnemyShipLevelThree()
        {
            var enemyShipLvl3 = new EnemyShipLevel3();
            return enemyShipLvl3;
        }

        /// <summary>
        ///     Creates the level four enemy ship.
        ///     Precondition: None
        ///     Postcondition: Enemy ship level four is created
        /// </summary>
        /// <returns>A new instance of a level four enemy ship</returns>
        public static EnemyShipLevel4 CreateEnemyShipLevelFour()
        {
            var enemyShipLvl4 = new EnemyShipLevel4();
            return enemyShipLvl4;
        }

        /// <summary>
        ///     Creates the bonus ship.
        ///     Precondition: None
        ///     Postcondition: Bonus ship is created
        /// </summary>
        /// <returns>A new instance of a bonus ship</returns>
        public static BonusShip CreateBonusShip()
        {
            var bonusShip = new BonusShip();
            return bonusShip;
        }

        /// <summary>
        ///     Creates the player ship.
        ///     Precondition: None
        ///     Postcondition: Player ship is created
        /// </summary>
        /// <returns>A new instance of a player ship</returns>
        public static PlayerShip CreatePlayerShip()
        {
            var playerShip = new PlayerShip();
            return playerShip;
        }

        /// <summary>
        /// Creates the ship.
        /// </summary>
        /// <param name="enemyShip">The enemy ship.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException"></exception>
        public EnemyShip CreateShip(Type enemyShip)
        {
            if (enemyShip == typeof(EnemyShipLevel1))
            {
                return CreateEnemyShipLevelOne();
            }
            else if (enemyShip == typeof(EnemyShipLevel2))
            {
                return CreateEnemyShipLevelTwo();
            }
            else if (enemyShip == typeof(EnemyShipLevel3))
            {
                return CreateEnemyShipLevelThree();
            }
            else if (enemyShip == typeof(EnemyShipLevel4))
            {
                return CreateEnemyShipLevelFour();
            }
            else if (enemyShip == typeof(BonusShip))
            {
                return CreateBonusShip();
            }

            throw new ArgumentException();
        }
    }
}