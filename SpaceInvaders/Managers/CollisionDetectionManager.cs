﻿using Windows.Foundation;
using Windows.UI.Xaml.Controls;
using SpaceInvaders.Model;

namespace SpaceInvaders.Managers
{
    /// <summary>
    ///     Controls the collision detection of sprites.
    /// </summary>
    public class CollisionDetectionManager
    {
        private readonly PlayerShip playerShip;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CollisionDetectionManager"/> class.
        /// </summary>
        public CollisionDetectionManager(PlayerShip playerShip)
        {
            this.playerShip = playerShip;
        }

        /// <summary>
        ///     Checks for a collision between a bullet and enemy ship sprite.
        ///     Precondition: Sprite opacity is less than or equal to 0
        ///     Postcondition: none
        /// </summary>
        /// <param name="background">The background.</param>
        /// <param name="bulletPosition">The bullet position.</param>
        /// <param name="enmyShip"></param>
        /// <returns>True if a bullet has collided with a ship, false otherwise.</returns>
        public bool EnemyShipCheckForCollision(Canvas background, Point bulletPosition, EnemyShip enmyShip)
        {
            var enemyShip = enmyShip;
            if (!enemyShip.IsThisShipAlive())
            {
                return false;
            }
            //var spriteLocation = enemyShip.Sprite.TransformToVisual(background).TransformPoint(new Point(0, 0));

            var left = enemyShip.X;
            var right = left + enemyShip.Sprite.Width;
            var top = enemyShip.Y;
            var bottom = top + enemyShip.Sprite.Height;

            var enemyShipType = enemyShip.GetType();

            if (bulletPosition.X > left && bulletPosition.X < right && bulletPosition.Y > top &&
                bulletPosition.Y < bottom)
            {
                if (enemyShipType == typeof(BonusShip))
                {
                    this.playerShip.InitiateTimer();
                }

                enemyShip.KillThisShip();
                return true;
            }
            return false;
        }

        /// <summary>
        ///     Checks for a collision between an enemy's bullet and the player ship
        /// <param name="background">The background.</param>
        /// <param name="bulletPosition">The bullet position.</param>
        /// <returns>True if there has been a collision, false otherwise</returns>
        /// </summary>
        public bool PlayerShipCheckForCollision(Canvas background, Point bulletPosition)
        {
            if (this.playerShip.Sprite.Opacity <= 0)
            {
                return false;
            }
            var spriteLocation = this.playerShip.Sprite.TransformToVisual(background).TransformPoint(new Point(0, 0));

            var left = spriteLocation.X;
            var right = left + this.playerShip.Sprite.Width;
            var top = spriteLocation.Y;
            var bottom = top + this.playerShip.Sprite.Height;

            if (!(bulletPosition.X > left) || !(bulletPosition.X < right) || !(bulletPosition.Y > top) ||
                !(bulletPosition.Y < bottom))
            {
                return false;
            }
            this.playerShip.Sprite.Opacity = 0;
            return true;
        }
    }
}