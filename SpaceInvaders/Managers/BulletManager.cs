﻿using System;
using System.Collections.Generic;
using Windows.Foundation;
using Windows.UI.Xaml.Controls;
using SpaceInvaders.Model;
using SpaceInvaders.Utilities;

namespace SpaceInvaders.Managers
{
    /// <summary>
    ///     Controls the creation and placement of bullets as well as the actions when a bullet hits something.
    /// </summary>
    public class BulletManager
    {
        private readonly List<Bullet> playerBullets;
        private readonly Canvas background;
        private readonly PlayerShip playerShip;
        private readonly EnemyFleetManager enemyFleet;
        private readonly CollisionDetectionManager collisionDetection;
        private readonly PlayerLifeManager playerLifeManager;
        private readonly ScoreManager scoreManager;
        private readonly SoundManager soundManager;
        private Bullet enemyBullet;
        private int randomBulletDelay;

        /// <summary>
        ///     The frequency of the dispatcher timer ticks, in milliseconds.
        /// </summary>
        public const int TickInterval = 500;

        /// <summary>
        ///     Prevents a default instance of the <see cref="BulletManager"/> class from being created.
        /// </summary>
        private BulletManager() { }

        /// <summary>
        ///     Initializes a new instance of the <see cref="BulletManager" /> class.
        /// </summary>
        /// <param name="theBackground">The theBackground.</param>
        /// <param name="playerShip">The player ship.</param>
        /// <param name="enemyFleetManager">The enemy fleet manager.</param>
        /// <param name="scoreManager">The score manager.</param>
        /// <param name="playerLifeManager">The player life manager.</param>
        /// <param name="soundManager">The sound manager.</param>
        /// <exception cref="ArgumentNullException"></exception>
        public BulletManager(Canvas theBackground, PlayerShip playerShip, EnemyFleetManager enemyFleetManager,
            ScoreManager scoreManager, PlayerLifeManager playerLifeManager, SoundManager soundManager)
        {
            if (theBackground == null)
            {
                throw new ArgumentNullException(nameof(theBackground));
            }
            this.background = theBackground;
            this.playerBullets = new List<Bullet>();
            this.playerShip = playerShip;
            this.enemyFleet = enemyFleetManager;
            this.collisionDetection = new CollisionDetectionManager(playerShip);
            this.playerLifeManager = playerLifeManager;
            this.scoreManager = scoreManager;
            this.soundManager = soundManager;
            this.resetRandomBulletDelay();
        }

        #region Player Bullet

        /// <summary>
        ///     Creates and places the player bullet in front and center of the player ship.
        ///     Precondition: playerBullet != null
        ///     Postcondition: playerBullet has been created and has been placed on the canvas.
        /// </summary>
        public void FirePlayerBullet()
        {
            var playerBullet = new Bullet();
            if (this.playerShip.PowerUp && this.playerBullets.Count < 5)
            {
                this.firePlayerBulletOnIntervalWithSound(playerBullet);
            }
            else if (!this.playerShip.PowerUp && this.playerBullets.Count < 3)
            {
                this.firePlayerBulletOnIntervalWithSound(playerBullet);
            }
        }

        private void firePlayerBulletOnIntervalWithSound(Bullet playerBullet)
        {
            this.playerBullets.Add(playerBullet);
            ThreadManager.DelayTaskWithWait(() => this.placePlayerBullet(playerBullet), TickInterval + 100);
            this.soundManager.PlayPlayerFiringSound();
        }

        /// <summary>
        ///     Moves the player bullet up.
        ///     Precondition: None
        ///     Postcondition: The player bullet moves up
        /// </summary>
        public void MovePlayerBulletUp()
        {
            foreach (var playerBullet in this.playerBullets)
            {
                if (this.background.Children.Contains(playerBullet.Sprite))
                {
                    playerBullet.MoveUp();
                }
            }
            this.bulletBoundaryCollisionDetection();
        }

        /// <summary>
        ///     Determines if a player bullet has hit an enemy ship.
        ///     Precondition: None
        ///     Postcondition: The enemy ship is removed, the player's score is updated, and the player's bullet is removed.
        /// </summary>
        /// <param name="soundMgr">The sound manager.</param>
        public void EnemyShipHitWithPlayerBullet(SoundManager soundMgr)
        {
            for (var i = 0; i < this.enemyFleet.GetFleetCount(); i++)
            {
                this.playerBulletHitAction(i, soundMgr);
            }
        }

        private void placePlayerBullet(GameObject playerBullet)
        {
            this.background.Children.Add(playerBullet.Sprite);

            playerBullet.X = this.playerShip.Width / 2 + this.playerShip.X;
            playerBullet.Y = this.playerShip.Y;
        }

        private void bulletBoundaryCollisionDetection()
        {
            for (var i = 0; i < this.playerBullets.Count; i++)
            {
                if (this.playerBullets[i] == null || !(this.playerBullets[i].Y < 0))
                {
                    continue;
                }
                this.removePlayerBullet(i);
                i--;
            }
        }

        private void removePlayerBullet(int index)
        {
            this.background.Children.Remove(this.playerBullets[index].Sprite);
            this.playerBullets.RemoveAt(index);
        }

        private void removeAllPlayerBullets()
        {
            for (var i = 0; i < this.playerBullets.Count; i++)
            {
                this.removePlayerBullet(i);
                i--;
            }
        }

        private void playerBulletHitAction(int enemyShipIndex, SoundManager sndManager)
        {
            var enemyShip = this.enemyFleet.GetEnemyShipAt(enemyShipIndex);
            var sound = sndManager;
            for (var i = 0; i < this.playerBullets.Count; i++)
            {
                if (!this.collisionDetection.EnemyShipCheckForCollision(this.background,
                        new Point(this.playerBullets[i].X, this.playerBullets[i].Y), enemyShip))
                {
                    continue;
                }
                sound.PlayEnemyShipHitSound();
                this.removePlayerBullet(i);
                this.enemyFleet.RemoveEnemyShip(enemyShip);
                this.scoreManager.UpdateScore(enemyShip.GetShipPoints());

                i--;
            }
        }

        #endregion Player Bullet

        #region Enemy Bullet

        /// <summary>
        ///     Creates and places enemy bullets.
        ///     Precondition: Enemy bullet != null
        ///     Postcondition: The enemy bullet is created and has random firing capabilities.
        /// </summary>
        public void CreateAndPlaceEnemyBullets()
        {
            //this.BonusShipFiring(this.background);
            if (this.enemyBullet == null)
            {
                if (this.randomBulletDelay <= 0)
                {
                    this.resetRandomBulletDelay();
                    this.randomEnemyFiring(this.background);
                    this.soundManager.PlayEnemyFiringSound();
                }
                else
                {
                    this.randomBulletDelay--;
                }
            }
        }

        /// <summary>
        ///     Moves the enemy bullets down.
        ///     Precondition: None
        ///     Postcondition: Enemy bullet moves down and is removed from the theBackground if it hits the theBackground's bottom boundary.
        /// </summary>
        public void MoveEnemyBulletsDown()
        {
            this.enemyBullet?.MoveDown();
            if (this.enemyBullet?.Y > this.background.Height)
            {
                this.removeEnemyBullet();
            }
        }

        /// <summary>
        ///     Controls the hit action when a player ship is hit with an enemy bullet.
        ///     Precondition: enemyBullet != null and collision between player ship and bullet must have occurred.
        ///     Postcondition: Player loses a life
        /// </summary>
        public void PlayerShipHitWithEnemyBullet(SoundManager soundMgr)
        {
            var sound = soundMgr;
            if (this.enemyBullet == null || !this.collisionDetection.PlayerShipCheckForCollision(this.background, new Point(this.enemyBullet.X, this.enemyBullet.Y)))
            {
                return;
            }
            this.RemoveAllBullets();
            sound.PlayerShipHitSound();
            this.playerLifeManager.RemoveAPlayerLife();

            this.playerShip.Sprite.Opacity = 1.0;
        }

        /// <summary>
        ///     Randomizes the enemy firing.
        ///     Precondition: None
        ///     Postcondition: Enemy ships fire randomly
        /// </summary>
        /// <param name="theBackground">The theBackground.</param>
        private void randomEnemyFiring(Canvas theBackground)
        {
            var firingEnemyShips = this.enemyFleet.GetFiringShips();
            if (firingEnemyShips.Count <= 0)
            {
                return;
            }

            var rand = RandomGeneratorManager.Next(firingEnemyShips.Count);
            var ship = firingEnemyShips[rand];

            var spriteLocation = ship.Sprite.TransformToVisual(theBackground).TransformPoint(new Point(0, 0));
            var center = spriteLocation.X + ship.Width / 2;
            var bottom = spriteLocation.Y + ship.Sprite.Height;

            this.enemyBullet = new Bullet
            {
                X = center,
                Y = bottom
            };
            theBackground.Children.Add(this.enemyBullet.Sprite);
        }

        //public void BonusShipFiring(Canvas theBackground)
        //{
        //    var bonusShips = this.enemyFleet.GetBonusShip();
        //    if (bonusShips.Count <= 0)
        //    {
        //        return;
        //    }
        //    var bonusShip = bonusShips[0];

        //    var spriteLocation = bonusShip.Sprite.TransformToVisual(theBackground).TransformPoint(new Point(0, 0));
        //    var center = spriteLocation.X + bonusShip.Width / 2;
        //    var bottom = spriteLocation.Y + bonusShip.Sprite.Height;

        //    this.enemyBullet = new Bullet
        //    {
        //        X = center,
        //        Y = bottom
        //    };
        //    theBackground.Children.Add(this.enemyBullet.Sprite);
        //}

        private void resetRandomBulletDelay()
        {
            this.randomBulletDelay = RandomGeneratorManager.Next(3, 10);
        }

        private void removeEnemyBullet()
        {
            this.background.Children.Remove(this.enemyBullet.Sprite);
            this.enemyBullet = null;
        }

        #endregion Enemy Bullet

        /// <summary>
        /// Moves to next round.
        /// </summary>
        public void MoveToNextRound()
        {
            this.RemoveAllBullets();
        }

        /// <summary>
        ///     Removes all bullets from the game.
        ///     Precondition: None
        ///     Postcondition: All bullets are removed from the game
        /// </summary>
        public void RemoveAllBullets()
        {
            this.removeAllPlayerBullets();

            if (this.enemyBullet != null)
            {
                this.background.Children.Remove(this.enemyBullet.Sprite);
            }
        }
    }
}