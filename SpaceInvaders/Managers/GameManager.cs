﻿using System;
using System.Runtime.InteropServices;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using SpaceInvaders.Utilities;
using SpaceInvaders.View;

namespace SpaceInvaders.Managers
{
    /// <summary>
    ///     Manages the entire game.
    /// </summary>
    public class GameManager
    {
        #region Data members

        /// <summary>
        ///     The frequency of the dispatcher timer ticks, in milliseconds.
        /// </summary>
        public const int TickInterval = 500;

        private BulletManager bulletManager;
        private PlayerLifeManager playerLifeManager;
        private EnemyFleetManager enemyFleet;
        private ScoreManager scoreManager;
        private PlayerShipManager playerShipManager;
        private SoundManager soundManager;

        private readonly TimeSpan gameTickInterval = new TimeSpan(0, 0, 0, 0, TickInterval);

        private DispatcherTimer timer;
        private Canvas background;

        private double BackgroundHeight { get; set; }
        private double BackgroundWidth { get; set; }

        #endregion Data members

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="GameManager" /> class.
        ///     Precondition: backgroundHeight > 0 AND backgroundWidth > 0
        /// </summary>
        /// <param name="backgroundHeight">The backgroundHeight of the game play window.</param>
        /// <param name="backgroundWidth">The backgroundWidth of the game play window.</param>
        public GameManager(double backgroundHeight, double backgroundWidth)
        {
            if (backgroundHeight <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(backgroundHeight));
            }

            if (backgroundWidth <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(backgroundWidth));
            }

            this.BackgroundHeight = backgroundHeight;
            this.BackgroundWidth = backgroundWidth;
        }

        internal void MovePlayerShipLeft()
        {
            this.playerShipManager.MovePlayerShipLeft();
        }

        internal void MovePlayerShipRight()
        {
            this.playerShipManager.MovePlayerShipRight();
        }

        internal void FirePlayerBullet()
        {
            this.bulletManager.FirePlayerBullet();
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        ///     Initializes the game placing player ship and enemy ship in the game.
        ///     Precondition: background != null
        ///     Postcondition: Game is initialized and ready for play.
        /// </summary>
        /// <param name="theBackground">The background canvas.</param>
        /// <exception cref="ArgumentNullException"></exception>
        public void InitializeGame(Canvas theBackground)
        {
            if (theBackground == null)
            {
                throw new ArgumentNullException(nameof(theBackground));
            }

            if (this.background == null)
            {
                this.background = theBackground;
            }

            if (this.playerShipManager == null)
            {
                this.playerShipManager = new PlayerShipManager(this.background);
            }

            if (this.playerLifeManager == null)
            {
                this.playerLifeManager = new PlayerLifeManager(this.background);
                this.playerLifeManager.CreateAndPlacePlayerLives();
            }

            this.enemyFleet = new EnemyFleetManager(this.background);

            if (this.scoreManager == null)
            {
                this.scoreManager = new ScoreManager(this.background, this.enemyFleet);
            }

            this.enemyFleet.CreateAndPlaceEnemyShips(this.scoreManager);

            if (this.timer == null)
            {
                this.initializeTimer();
            }

            var temp = ThreadManager.UiTaskScheduler; // Initialize task scheduler (needs to initialize on ui thread)

            if (this.soundManager == null)
            {
                this.soundManager = new SoundManager();
            }

            this.bulletManager = new BulletManager(theBackground, this.playerShipManager.PlayerShip, this.enemyFleet,
                    this.scoreManager, this.playerLifeManager, this.soundManager);
        }

        internal void KillSomeEnemyShips()
        {
            this.enemyFleet.KillSomeEnemyShips();
        }

        internal void KillAllEnemyShips()
        {
            this.enemyFleet.KillAllEnemyShips();
        }

        private void timerOnTick(object sender, object o)
        {
            this.Tick();
        }

        /// <summary>
        ///     Controls the flow of the game on each tick of the timer.
        ///     Precondition: None
        ///     Postcondition: Game is created and continues.
        /// </summary>
        public void Tick()
        {
            this.enemyFleet.BonusShipCreationAndMovement(this.soundManager);
            this.enemyFleet.MoveEnemyShips();
            this.bulletManager.MovePlayerBulletUp();
            this.enemyFleet.AnimateEnemyShips();
            this.bulletManager.CreateAndPlaceEnemyBullets();
            this.bulletManager.MoveEnemyBulletsDown();
            this.bulletManager.EnemyShipHitWithPlayerBullet(this.soundManager);
            this.bulletManager.PlayerShipHitWithEnemyBullet(this.soundManager);
            this.hidePowerUpIndication();
            this.gameOver();
        }

        private async void gameOver()
        {
            if (this.scoreManager.CanMoveToNextLevel())
            {
                this.createNextRound();
            }
            else if (this.scoreManager.IsGameOver())
            {
                this.soundManager.BackgroundMusicStop();
                this.scoreManager.DisplayGameWonText();
                this.soundManager.PlayGameWonSound();
                this.timer.Stop();
                var scoreBoard = new HighScoreBoard();
                try
                {
                    await scoreBoard.ShowAsync();
                }
                catch (COMException )
                {

                }


            }
            else if (this.playerLifeManager.PlayerLivesLeft == 0)
            {
                this.soundManager.BackgroundMusicStop();
                this.scoreManager.DisplayGameOverText();
                this.timer.Stop();
                this.soundManager.PlayGameOverSound();
                var scoreBoard = new HighScoreBoard();
                await scoreBoard.ShowAsync();
                
            }
        }

        private void createNextRound()
        {
            this.scoreManager.RemoveGameOverText();
            this.bulletManager.RemoveAllBullets();
            this.InitializeGame(this.background);
            this.scoreManager.StartNewRound(this.enemyFleet);
        }

        private void initializeTimer()
        {
            if (this.timer == null)
            {
                this.timer = new DispatcherTimer();
            }
            this.timer.Tick += this.timerOnTick;
            this.timer.Interval = this.gameTickInterval;
            this.timer.Start();
        }

        private void hidePowerUpIndication()
        {
            if (!this.playerShipManager.PlayerShip.IsPowerUpActive())
            {
                this.scoreManager.RemovePowerUpText();
            }
            else
            {
                this.scoreManager.DisplayPowerUpText();
            }
        }

        #endregion Methods
    }
}