﻿using System;
using System.Collections.Generic;
using Windows.Media.Audio;
using Windows.Media.Render;
using Windows.Storage;

namespace SpaceInvaders.Managers
{
    /// <summary>
    ///     Controls the game sounds.
    /// </summary>
    public class SoundManager
    {
        private AudioGraph audioGraph;
        private AudioDeviceOutputNode deviceOutput;
        private readonly Dictionary<string, AudioFileInputNode> fileInputs = new Dictionary<string, AudioFileInputNode>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="SoundManager"/> class.
        /// </summary>
        public SoundManager()
        {
            this.soundLoaded();
            //this.BackgroundMusicStart();
        }

        private async void soundLoaded()
        {
            await this.initializeSound();
            this.BackgroundMusicStart();
        }

        /// <summary>
        ///     Plays the enemy firing sound.
        ///     Precondition: None
        ///     Postcondition: When enemies fire bullets, a sound is played
        /// </summary>
        public void PlayEnemyFiringSound()
        {
            var firingSound = this.fileInputs["Turret_deploy.wav"];
            firingSound.Reset();
            firingSound.Start();
        }

        /// <summary>
        ///     Plays the game over sound.
        ///     Precondition: None
        ///     Postcondition: A sound is played when the game is over.
        /// </summary>
        public void PlayGameOverSound()
        {
            var gameOver = this.fileInputs["Nevermore.mp3"];
            gameOver.Reset();
            gameOver.Start();
        }

        /// <summary>
        ///     Plays the game won sound.
        ///     Precondition: None
        ///     Postcondition: A sound is made when the game is won.
        /// </summary>
        public void PlayGameWonSound()
        {
            var gameWon = this.fileInputs["AlagaesiaAndEragon.mp3"];
            gameWon.Reset();
            gameWon.Start();
        }

        /// <summary>
        ///     Plays the player firing sound.
        ///     Precondition: None
        ///     Postcondition: Player ship makes a sound when firing
        /// </summary>
        public void PlayPlayerFiringSound()
        {
            var playerShipFire = this.fileInputs["Turret_alarm.wav"];
            playerShipFire.Reset();
            playerShipFire.Start();
        }

        /// <summary>
        ///     Plays the player ship hit sound.
        ///     Precondition: None
        ///     Postcondition: A sound is made when the player ship is hit.
        /// </summary>
        public void PlayerShipHitSound()
        {
            var playerShipHit = this.fileInputs["Turret_die.wav"];
            playerShipHit.Reset();
            playerShipHit.Start();
        }

        /// <summary>
        ///     Plays the enemy ship hit sound.
        ///     Precondition: None
        ///     Postcondition: A sound is played when the enemy ship is hit.
        /// </summary>
        public void PlayEnemyShipHitSound()
        {
            var enemyShipHit = this.fileInputs["Turret_retract.wav"];
            enemyShipHit.Reset();
            enemyShipHit.Start();
        }

        /// <summary>
        ///     Plays a sound when the bonus enemy ship is active.
        ///     Precondition: None
        ///     Postcondition: A sound is played when the bonus enemy ship flies around.
        /// </summary>
        public void BonusEnemyShipSound()
        {
            var bonusShip = this.fileInputs["enemyattack.wav"];
            bonusShip.Reset();
            bonusShip.Start();
        }

        /// <summary>
        ///     Starts playing the background music.
        ///     Precondition: None
        ///     Postcondition: Background music begins playing.
        /// </summary>
        public void BackgroundMusicStart()
        {
            var backgroundMusic = this.fileInputs["Surreal_Chase_Looping.mp3"];
            backgroundMusic.Reset();
            backgroundMusic.Start();
            backgroundMusic.LoopCount = null;
        }

        /// <summary>
        ///     Stops playing the background music.
        ///     Precondition: None
        ///     Postcondition: Background music is no longer played.
        /// </summary>
        public void BackgroundMusicStop()
        {
            var backgroundMusic = this.fileInputs["Surreal_Chase_Looping.mp3"];
            backgroundMusic.Reset();
            backgroundMusic.Stop();
        }

        private async System.Threading.Tasks.Task initializeSound()
        {
            var settings = new AudioGraphSettings(AudioRenderCategory.Media);
            var result = await AudioGraph.CreateAsync(settings);

            if (result.Status == AudioGraphCreationStatus.Success)
            {
                this.audioGraph = result.Graph;

                var deviceOutputNodeResult =
                    await this.audioGraph.CreateDeviceOutputNodeAsync();

                if (deviceOutputNodeResult.Status == AudioDeviceNodeCreationStatus.Success)
                {
                    this.deviceOutput = deviceOutputNodeResult.DeviceOutputNode;
                    this.audioGraph.ResetAllNodes();

                    await this.addFileToSounds("ms-appx:///Assets/Turret_deploy.wav");
                    await this.addFileToSounds("ms-appx:///Assets/Nevermore.mp3");
                    await this.addFileToSounds("ms-appx:///Assets/Turret_alarm.wav");
                    await this.addFileToSounds("ms-appx:///Assets/AlagaesiaAndEragon.mp3");
                    await this.addFileToSounds("ms-appx:///Assets/Turret_die.wav");
                    await this.addFileToSounds("ms-appx:///Assets/Turret_retract.wav");
                    await this.addFileToSounds("ms-appx:///Assets/Surreal_Chase_Looping.mp3");
                    await this.addFileToSounds("ms-appx:///Assets/enemyattack.wav");

                    this.audioGraph.Start();
                }
            }
        }

        private async System.Threading.Tasks.Task addFileToSounds(string uri)
        {
            var soundFile = await StorageFile.GetFileFromApplicationUriAsync(new Uri(uri));
            var fileInputResult = await this.audioGraph.CreateFileInputNodeAsync(soundFile);

            if (AudioFileNodeCreationStatus.Success == fileInputResult.Status)
            {
                this.fileInputs.Add(soundFile.Name, fileInputResult.FileInputNode);
                fileInputResult.FileInputNode.Stop();
                fileInputResult.FileInputNode.AddOutgoingConnection(this.deviceOutput);
            }
        }
    }
}