﻿using System;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace SpaceInvaders.Managers
{
    /// <summary>
    ///     Controls the game score.
    /// </summary>
    public class ScoreManager
    {
        private EnemyFleetManager enemyFleet;
        private readonly TextBlock scoreText;
        private readonly TextBlock gameOverText;
        private readonly TextBlock powerUpText;

        private const int GameOverTextPlacementY = 350;
        private const int GameOverTextPlacementX = 70;
        private const int ScorePlacementY = 300;
        private const int ScorePlacementX = 50;
        private const int PowerUpTextPlacementY = 425;
        private const int PowerUpTextPlacementX = 20;

        /// <summary>
        ///     Gets or sets the game level.
        /// </summary>
        /// <value>
        /// The game level.
        /// </value>
        public int GameLevel { get; set; } = 1;

        /// <summary>
        /// Gets or sets the total points.
        /// </summary>
        /// <value>
        /// The total points.
        /// </value>
        public int TotalPoints { get; set; }

        /// <summary>
        ///     Prevents a default instance of the <see cref="ScoreManager"/> class from being created.
        /// </summary>
        private ScoreManager() { }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ScoreManager"/> class.
        /// </summary>
        /// <param name="theBackground">The background.</param>
        /// <param name="enemyFleetManager"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public ScoreManager(Canvas theBackground, EnemyFleetManager enemyFleetManager)
        {
            if (theBackground == null)
            {
                throw new ArgumentNullException(nameof(theBackground));
            }
            var background = theBackground;

            this.enemyFleet = enemyFleetManager;
            this.TotalPoints = 0;

            this.scoreText = new TextBlock { Foreground = new SolidColorBrush(Colors.AntiqueWhite) };
            background.Children.Add(this.scoreText);
            Canvas.SetTop(this.scoreText, ScorePlacementY);
            Canvas.SetLeft(this.scoreText, ScorePlacementX);

            this.gameOverText = new TextBlock { Foreground = new SolidColorBrush(Colors.AntiqueWhite) };
            background.Children.Add(this.gameOverText);
            Canvas.SetTop(this.gameOverText, GameOverTextPlacementY);
            Canvas.SetLeft(this.gameOverText, GameOverTextPlacementX);

            this.powerUpText = new TextBlock {
                Foreground = new SolidColorBrush(Colors.Blue),
                Visibility = Visibility.Collapsed,
                Text = "POWER UP ACTIVATED"
            };
            background.Children.Add(this.powerUpText);
            Canvas.SetTop(this.powerUpText, PowerUpTextPlacementY);
            Canvas.SetLeft(this.powerUpText, PowerUpTextPlacementX);
        }

        /// <summary>
        ///     Determines whether the game has been won by checking if all enemies have died.
        ///     Precondition: None
        ///     Postcondition: If game has been won, then text appears saying that game has been won.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if [has game been won]; otherwise, <c>false</c>.
        /// </returns>
        public bool HasRoundBeenWon(int currentGameRound)
        {
            return this.enemyFleet.HasKilledAllEnemies();
        }

        /// <summary>
        ///     Determines whether the game is over.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if the game level > 3 and all player ships have been killed; otherwise, <c>false</c>.
        /// </returns>
        public bool IsGameOver()
        {
            return this.GameLevel > 2 && this.enemyFleet.HasKilledAllEnemies();
        }

        /// <summary>
        ///     Displays the game won text.
        ///     Precondition: None
        ///     Postcondition: The game won text displays.
        /// </summary>
        public void DisplayGameWonText()
        {
            this.gameOverText.Text = "GAME WON";
        }

        /// <summary>
        ///     Sets the game over text.
        ///     Precondition: None
        ///     Postcondition: The game over text displays.
        /// </summary>
        public void DisplayGameOverText()
        {
            this.gameOverText.Text = "GAME OVER";
        }

        /// <summary>
        ///     Updates the score text.
        ///     Precondition: None
        ///     Postcondition: Score text has been updated
        /// </summary>
        public void UpdateScore(int points)
        {
            this.TotalPoints += points;
            this.scoreText.Text = "Score: " + this.TotalPoints;
        }

        /// <summary>
        ///     Displays the power up text.
        ///     Precondition: None
        ///     Postcondition: The power up text displays.
        /// </summary>
        public void DisplayPowerUpText()
        {
            this.powerUpText.Visibility = Visibility.Visible;
        }

        /// <summary>
        ///     Changes the power up text to an empty string so that it can be replaced with a new score.
        ///     Precondition: None
        ///     Postcondition: power up text has been updated
        /// </summary>
        public void RemovePowerUpText()
        {
            this.powerUpText.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        ///     Changes the score text to an empty string so that it can be replaced with a new score.
        ///     Precondition: None
        ///     Postcondition: Score text has been updated
        /// </summary>
        public void RemoveScore()
        {
            this.scoreText.Text = "";
        }

        /// <summary>
        ///     Removes the game over text.
        ///     Precondition: None
        ///     Postcondition: Score text has been updated
        /// </summary>
        public void RemoveGameOverText()
        {
            this.gameOverText.Text = "";
        }

        /// <summary>
        ///     Determines whether the player can move to the next level.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance [can move to next level]; otherwise, <c>false</c>.
        /// </returns>
        public bool CanMoveToNextLevel()
        {
            if (!this.HasRoundBeenWon(this.GameLevel) || this.GameLevel >= 3)
            {
                return false;
            }
            this.updateGameRound();
            return true;
        }

        private void updateGameRound()
        {
            this.GameLevel++;
        }

        internal void StartNewRound(EnemyFleetManager enmyFleet)
        {
            this.enemyFleet = enmyFleet;
        }
    }
}