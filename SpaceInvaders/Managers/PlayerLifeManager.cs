﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml.Controls;
using SpaceInvaders.Model;

namespace SpaceInvaders.Managers
{
    /// <summary>
    ///     Controls the creation, placement, and removal of player lives.
    /// </summary>
    public class PlayerLifeManager
    {
        private readonly List<PlayerLife> playerLives;
        private readonly Canvas background;

        /// <summary>
        ///     Gets or sets the player lives left.
        /// </summary>
        /// <value>
        /// The player lives left.
        /// </value>
        public int PlayerLivesLeft { get; set; } = 3;

        /// <summary>
        ///     Prevents a default instance of the <see cref="PlayerLifeManager"/> class from being created.
        /// </summary>
        private PlayerLifeManager() { }

        /// <summary>
        ///     Initializes a new instance of the <see cref="PlayerLifeManager"/> class.
        /// </summary>
        /// <param name="theBackground">The background.</param>
        /// <exception cref="ArgumentNullException"></exception>
        public PlayerLifeManager(Canvas theBackground)
        {
            if (theBackground == null)
            {
                throw new ArgumentNullException(nameof(theBackground));
            }
            this.background = theBackground;
            this.playerLives = new List<PlayerLife>();
        }

        /// <summary>
        ///     Creates and places player lives.
        ///     Precondition: None
        ///     Postcondition: Three player lives are created and placed on the canvas
        /// </summary>
        public void CreateAndPlacePlayerLives()
        {
            this.addPlayerLives();
            foreach (var playerLife in this.playerLives)
            {
                this.background.Children.Add(playerLife.Sprite);
                Canvas.SetTop(playerLife.Sprite, 460);
            }

            Canvas.SetLeft(this.playerLives[0].Sprite, 20);
            Canvas.SetLeft(this.playerLives[1].Sprite, 40);
            Canvas.SetLeft(this.playerLives[2].Sprite, 60);
        }

        /// <summary>
        ///     Removes a player life
        /// </summary>
        public void RemoveAPlayerLife()
        {
            var currentLife = this.playerLives.Count - 1;
            this.playerLives[currentLife].Sprite.Opacity = 0;
            this.background.Children.Remove(this.playerLives[currentLife].Sprite);
            this.playerLives.RemoveAt(currentLife);

            this.PlayerLivesLeft = this.PlayerLivesLeft - 1;
        }

        /// <summary>
        ///     Removes all player lives.
        ///     Precondition: None
        ///     Postcondition: All player lives are removed
        /// </summary>
        public void RemoveAllPlayerLives()
        {
            for (var i = 0; i < this.playerLives.Count; i++)
            {
                this.playerLives[i].Sprite.Opacity = 0;
                this.background.Children.Remove(this.playerLives[i].Sprite);
                this.playerLives.RemoveAt(i);
            }
        }

        private void addPlayerLives()
        {
            for (var i = 0; i < this.PlayerLivesLeft; i++)
            {
                var playerLife = new PlayerLife();
                this.playerLives.Add(playerLife);
            }
        }
    }
}