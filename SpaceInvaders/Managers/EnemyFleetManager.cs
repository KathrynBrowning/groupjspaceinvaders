﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml.Controls;
using SpaceInvaders.Model;
using SpaceInvaders.Utilities;

namespace SpaceInvaders.Managers
{
    /// <summary>
    ///     Controls all actions of the enemy fleet
    /// </summary>
    public class EnemyFleetManager
    {
        private int enemyShipTickCount = 20;
        private int count;
        private int bonusShipRandomDelay;
        private const int EnemyShipSpeed = 5;
        private int enemyShipMovement = EnemyShipSpeed;

        private const int LevelFourEnemyShipMaxY = 40;
        private const int LevelThreeEnemyShipMaxY = 200;
        private const int LevelTwoEnemyShipMaxYOnFinalLevel = 380;
        private const int LevelTwoEnemyShipMaxY = 380;
        private const int LevelOneEnemyShipMaxY = 420;

        private const int LevelOneEnemyShipsYReset = 45;
        private const int LevelTwoEnemyShipsYReset = 75;
        private const int LevelThreeEnemyShipsYReset = 65;
        private const int LevelFourEnemyShipsYReset = 3;

        private const int BonusShipSpeed = 5;
        private const int BonusShipMovement = BonusShipSpeed;

        private const int DistanceBetweenShipOffset = 15;

        private readonly List<EnemyShip> enemyFleet;
        private ScoreManager scoreManager;

        private readonly Canvas background;

        private readonly double firstLevelEnemyShipYLocation;
        private readonly double secondLevelEnemyShipYLocation;
        private readonly double thirdLevelEnemyShipYLocation;
        private readonly double fourthLevelEnemyShipYLocation;

        /// <summary>
        ///     Prevents a default instance of the <see cref="EnemyFleetManager"/> class from being created.
        /// </summary>
        private EnemyFleetManager()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="EnemyFleetManager"/> class.
        /// </summary>
        public EnemyFleetManager(Canvas theBackground)
        {
            if (theBackground == null)
            {
                throw new ArgumentNullException(nameof(theBackground));
            }
            this.background = theBackground;
            this.enemyFleet = new List<EnemyShip>();

            this.firstLevelEnemyShipYLocation = this.background.Height / 1.9;
            this.secondLevelEnemyShipYLocation = this.background.Height / 2.4;
            this.thirdLevelEnemyShipYLocation = this.background.Height / 4;
            this.fourthLevelEnemyShipYLocation = this.background.Height / 10;
        }

        /// <summary>
        ///     Moves to next round.
        ///     Precondition: None
        ///     Postcondition: Creates and places enemy ships
        /// </summary>
        public void MoveToNextRound()
        {
            this.CreateAndPlaceEnemyShips(this.scoreManager);
        }

        #region Ship Placement

        /// <summary>
        ///     Creates and places enemy ships.
        ///     Precondition: None
        ///     Postcondition: Enemy ships have been created and placed
        /// </summary>
        public void CreateAndPlaceEnemyShips(ScoreManager screManager)
        {
            this.scoreManager = screManager;
            this.placeEnemyShips(typeof(EnemyShipLevel1), 2, this.firstLevelEnemyShipYLocation);
            this.placeEnemyShips(typeof(EnemyShipLevel2), 4, this.secondLevelEnemyShipYLocation);
            this.placeEnemyShips(typeof(EnemyShipLevel3), 6, this.thirdLevelEnemyShipYLocation);
            this.placeEnemyShips(typeof(EnemyShipLevel4), 8, this.fourthLevelEnemyShipYLocation);

            this.resetBonusShipRandomDelay();
        }

        /// <summary>
        ///     Moves the enemy ships.
        ///     Precondition: None
        ///     Postcondition: Enemy ships move towards the opposite direction when they near the canvas edge
        /// </summary>
        public void MoveEnemyShips()
        {
            this.enemyShipTickCount++;

            if (this.scoreManager.GameLevel == 1)
            {
                foreach (var enemyShip in this.NornalEnemyShips())
                {
                    this.normalShipMovement(enemyShip);
                }
            }

            this.gameLevelTwoEnemyShipMovements();

            if (this.scoreManager.GameLevel == 3)
            {
                foreach (var enemyShip in this.NornalEnemyShips())
                {
                    var enemyShipType = enemyShip.GetType();

                    if (enemyShipType == typeof(EnemyShipLevel1))
                    {
                        this.normalShipMovement(enemyShip);

                        if (this.enemyShipTickCount < 20)
                        {
                            if (enemyShip.Y < LevelOneEnemyShipMaxY)
                            {
                                enemyShip.Y = enemyShip.Y + 3;
                            }
                            else
                            {
                                enemyShip.Y = enemyShip.Y - LevelOneEnemyShipsYReset;
                            }
                        }

                    }
                    else if (enemyShipType == typeof(EnemyShipLevel2))
                    {

                        this.normalShipMovement(enemyShip);

                        if (this.enemyShipTickCount < 20)
                        {
                            if (enemyShip.Y < LevelTwoEnemyShipMaxYOnFinalLevel)
                            {
                                enemyShip.Y = enemyShip.Y + 4;
                            }
                            else
                            {
                                enemyShip.Y = enemyShip.Y - LevelTwoEnemyShipsYReset;
                            }
                        }

                    }
                    else if (enemyShipType == typeof(EnemyShipLevel3))
                    {
                        this.normalShipMovement(enemyShip);

                        if (this.enemyShipTickCount < 20)
                        {
                            if (enemyShip.Y  < LevelThreeEnemyShipMaxY)
                            {
                                enemyShip.Y = enemyShip.Y + 1;
                            }
                            else
                            {
                                enemyShip.Y = enemyShip.Y - LevelThreeEnemyShipsYReset;
                            }
                            
                        }

                    }
                    else if (enemyShipType == typeof(EnemyShipLevel4))
                    {

                        this.normalShipMovement(enemyShip);

                        if (this.enemyShipTickCount < 20)
                        {
                            if (enemyShip.Y > LevelFourEnemyShipMaxY)
                            {
                                enemyShip.Y = enemyShip.Y - 1;
                            }
                            else
                            {
                                enemyShip.Y = enemyShip.Y + LevelFourEnemyShipsYReset;
                            }
                        }

                    }
                }
            }
        }

        private void gameLevelTwoEnemyShipMovements()
        {
            if (this.scoreManager.GameLevel == 2)
            {
                foreach (var enemyShip in this.NornalEnemyShips())
                {
                    var enemyShipType = enemyShip.GetType();

                    if (enemyShipType == typeof(EnemyShipLevel1))
                    {
                        this.normalShipMovement(enemyShip);

                        if (this.enemyShipTickCount < 20)
                        {
                            if (enemyShip.Y < LevelOneEnemyShipMaxY)
                            {
                                enemyShip.Y = enemyShip.Y + 1;
                            }
                        }
                        else if (this.enemyShipTickCount > 20)
                        {
                            enemyShip.Y = enemyShip.Y - 1;
                        }
                    }
                    if (enemyShipType == typeof(EnemyShipLevel2))
                    {
                        this.normalShipMovement(enemyShip);

                        if (this.enemyShipTickCount < 20)
                        {
                            if (enemyShip.Y < LevelTwoEnemyShipMaxY)
                            {
                                enemyShip.Y = enemyShip.Y + 1;
                            }
                        }
                    }
                    else if (enemyShipType == typeof(EnemyShipLevel3) || enemyShipType == typeof(EnemyShipLevel4))
                    {
                        this.normalShipMovement(enemyShip);
                    }
                }
            }
        }

        private void normalShipMovement(EnemyShip enemyShip)
        {
            if (this.enemyShipMovement > 0 &&
                this.enemyShipTickCount >= (this.background.Width - (enemyShip.X + enemyShip.Width)))
            {
                this.enemyShipTickCount = 0;
                this.enemyShipMovement = -this.enemyShipMovement;
            }
            else if (this.enemyShipMovement < 0 && this.enemyShipTickCount > enemyShip.X)
            {
                this.enemyShipTickCount = 0;
                this.enemyShipMovement = -this.enemyShipMovement;
            }
            enemyShip.X += this.enemyShipMovement;
        }

        /// <summary>
        ///     Removes the specified enemy ship.
        /// </summary>
        /// <param name="enemyShip">The enemy ship.</param>
        public void RemoveEnemyShip(EnemyShip enemyShip)
        {
            this.background.Children.Remove(enemyShip.Sprite);
            this.enemyFleet.Remove(enemyShip);
        }

        private void placeEnemyShips(Type enemyShipType, int numberOfShips, double yValue)
        {
            var isBonusShip = enemyShipType == typeof(BonusShip);
            var shipFactory = new ShipFactory();
            var ship = shipFactory.CreateShip(enemyShipType);
            var startingX = this.setShipStartingPosition(numberOfShips, ship);

            if (isBonusShip)
            {
                startingX = 10;
            }

            var currentX = startingX;

            for (var i = 0; i < numberOfShips; i++)
            {
                var enemyShip = shipFactory.CreateShip(enemyShipType);
                enemyShip.PlaceEnemyShip(currentX, yValue);
                currentX = currentX + enemyShip.Width + DistanceBetweenShipOffset / 2.0;
                this.enemyFleet.Add(enemyShip);
                this.background.Children.Add(enemyShip.Sprite);
            }
        }

        private double setShipStartingPosition(int numberOfShips, GameObject ship)
        {
            if (this.scoreManager.GameLevel == 1)
            {
                var levelOneX = this.background.Width / 2 - numberOfShips / 2.0 * ship.Width -
                                ((numberOfShips / 2) - 1) * DistanceBetweenShipOffset - DistanceBetweenShipOffset / 2.0;
                return levelOneX;
            }
            else if (this.scoreManager.GameLevel == 2)
            {
                return this.setGameLevelTwoShipPositions(ship, 6, 8);
            }
            else if (this.scoreManager.GameLevel == 3)
            {
                return this.setGameLevelThreeShipPositions(ship, 6, 8);
            }
            return -1;
        }

        private double setGameLevelTwoShipPositions(GameObject ship, int numberOfLevel3Ships, int numberOfLevel4Ships)
        {
            var levelTwoX = 0.0;
            if (ship is EnemyShipLevel1)
            {
                levelTwoX = (this.background.Width / 4.0);
            }
            else if (ship is EnemyShipLevel2)
            {
                levelTwoX = (this.background.Width / 2.0) - ship.Width;
            }
            else if (ship is EnemyShipLevel3)
            {
                levelTwoX = this.background.Width / 2 - numberOfLevel3Ships / 2.0 * ship.Width -
                                 ((numberOfLevel3Ships / 2) - 1) * DistanceBetweenShipOffset - DistanceBetweenShipOffset / 2.0;
            }
            else if (ship is EnemyShipLevel4)
            {
                levelTwoX = this.background.Width / 2 - numberOfLevel4Ships / 2.0 * ship.Width -
                                 ((numberOfLevel4Ships / 2) - 1) * DistanceBetweenShipOffset - DistanceBetweenShipOffset / 2.0;
            }
            return levelTwoX;
        }

        private double setGameLevelThreeShipPositions(GameObject ship, int numberOfLevel3Ships, int numberOfLevel4Ships)
        {
            var levelThreeX = 0.0;
            if (ship is EnemyShipLevel1)
            {
                levelThreeX = (this.background.Width / 5.0) - ship.Width / 2.0;
            }
            else if (ship is EnemyShipLevel2)
            {
                levelThreeX = (this.background.Width / 2.0) - ship.Width / 2.0;
            }
            else if (ship is EnemyShipLevel3)
            {
                levelThreeX = this.background.Width / 2 - numberOfLevel3Ships / 2.0 * ship.Width -
                                 ((numberOfLevel3Ships / 2) - 1) * DistanceBetweenShipOffset - DistanceBetweenShipOffset / 2.0;
            }
            else if (ship is EnemyShipLevel4)
            {
                levelThreeX = this.background.Width / 2 - numberOfLevel4Ships / 2.0 * ship.Width -
                                 ((numberOfLevel4Ships / 2) - 1) * DistanceBetweenShipOffset - DistanceBetweenShipOffset / 2.0;
            }
            return levelThreeX;
        }

        #region Bonus Ship

        private EnemyShip getBonusShip()
        {
            return this.enemyFleet.Find(s => s.GetType() == typeof(BonusShip));
        }

        /// <summary>
        ///     Creates and moves the bonus ship.
        ///     Precondition: None
        ///     Postcondition: Bonus ship is created and can move.
        /// </summary>
        public void BonusShipCreationAndMovement(SoundManager soundManager)
        {
            this.createAndPlaceBonusShip();
            this.moveBonusShip();
        }

        private void createAndPlaceBonusShip()
        {
            var bonusShip = this.getBonusShip();

            if (this.bonusShipRandomDelay <= 0 && bonusShip == null)
            {
                this.resetBonusShipRandomDelay();
                const int bonusEnemyShipYLocation = 10;
                this.placeEnemyShips(typeof(BonusShip), 1, bonusEnemyShipYLocation);
            }
            else if (bonusShip == null)
            {
                this.bonusShipRandomDelay--;
            }
        }

        private void moveBonusShip()
        {
            var bonusShip = this.getBonusShip();

            if (bonusShip == null)
            {
                return;
            }

            if (bonusShip.X >= this.background.Width)
            {
                this.background.Children.Remove(bonusShip.Sprite);
                this.enemyFleet.Remove(bonusShip);
                this.resetBonusShipRandomDelay();
            }
            else
            {
                bonusShip.X += BonusShipMovement;
            }
        }

        private void resetBonusShipRandomDelay()
        {
            this.bonusShipRandomDelay = RandomGeneratorManager.Next(10) + 10;
        }

        #endregion Bonus Ship

        #endregion Ship Placement

        #region Kill Ships

        /// <summary>
        ///     Determines whether all enemy ships have been killed.
        ///     Precondition: None
        ///     Postcondition: None
        /// </summary>
        /// <returns>
        ///   <c>true</c> if all enemy ships have been killed, otherwise, <c>false</c>.
        /// </returns>
        public bool HasKilledAllEnemies()
        {
            return this.enemyFleet.Count <= 0;
        }

        /// <summary>
        ///     Kills all enemy ships. FOR TESTING PURPOSES.
        ///     Precondition: None
        ///     Postcondition: All enemy ships are terminated
        /// </summary>
        public void KillAllEnemyShips()
        {
            for (var i = 0; i < this.enemyFleet.Count; i++)
            {
                this.enemyFleet[i].KillThisShip();
                this.enemyFleet.RemoveAt(i);
                i--;
            }
        }

        /// <summary>
        ///     Kills some enemy ships. FOR TESTING PURPOSES.
        ///     Precondition: None
        ///     Postcondition: Kills half of the enemy ships
        /// </summary>
        public void KillSomeEnemyShips()
        {
            for (var i = 0; i < this.enemyFleet.Count; i++)
            {
                this.enemyFleet[i].KillThisShip();
                this.enemyFleet.RemoveAt(i);
            }
        }

        #endregion Kill Ships

        #region Ship Movement

        /// <summary>
        ///     Animates the enemy ships.
        ///     Precondition: None
        ///     Postcondition: Animates each enemy ship
        /// </summary>
        public void AnimateEnemyShips()
        {
            this.count++;
            if (this.count <= 5)
            {
                return;
            }
            foreach (var enemyShip in this.enemyFleet)
            {
                enemyShip.Animate();
            }
            this.count = 0;
        }

        #endregion Ship Movement

        /// <summary>
        ///     Gets the fleet count.
        ///     Precondition: None
        ///     Postcondition: None
        /// </summary>
        /// <returns>The number of ships in the fleet</returns>
        public int GetFleetCount()
        {
            return this.enemyFleet.Count;
        }

        /// <summary>
        ///     Gets the enemy ship at the specified index.
        ///     Precondition: None
        ///     Postcondition: None
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public EnemyShip GetEnemyShipAt(int index)
        {
            return this.enemyFleet[index];
        }

        internal List<EnemyShip> GetFiringShips()
        {
            return this.enemyFleet.FindAll(s => (s.GetType() == typeof(EnemyShipLevel3) || s.GetType() == typeof(EnemyShipLevel4)) || s.GetType() == typeof(BonusShip) && s.IsAlive);
        }

        internal List<EnemyShip> NornalEnemyShips()
        {
            return this.enemyFleet.FindAll(s => (s.GetType() == typeof(EnemyShipLevel1) || s.GetType() == typeof(EnemyShipLevel2) || s.GetType() == typeof(EnemyShipLevel3)) || s.GetType() == typeof(EnemyShipLevel4) && s.IsAlive);
        }
    }
}