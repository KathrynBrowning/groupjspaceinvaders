﻿using System;
using Windows.UI.Xaml.Controls;
using SpaceInvaders.Model;
using SpaceInvaders.Utilities;

namespace SpaceInvaders.Managers
{
    /// <summary>
    ///     Controls the creation and placement of player ships.
    /// </summary>
    public class PlayerShipManager
    {
        /// <summary>
        /// The player ship
        /// </summary>
        public PlayerShip PlayerShip;

        private readonly Canvas background;
        private const double PlayerShipBottomOffset = 5;

        ///// <summary>
        /////     Gets or sets the height of the background.
        ///// </summary>
        ///// <value>
        /////     The height of the background.
        ///// </value>
        //public double BackgroundHeight { get; set; }

        ///// <summary>
        /////     Gets or sets the width of the background.
        ///// </summary>
        ///// <value>
        /////     The width of the background.
        ///// </value>
        //public double BackgroundWidth { get; set; }

        /// <summary>
        ///     Prevents a default instance of the <see cref="PlayerShipManager"/> class from being created.
        /// </summary>
        private PlayerShipManager() { }

        /// <summary>
        ///     Initializes a new instance of the <see cref="PlayerShipManager"/> class.
        /// </summary>
        /// <param name="theBackground">The background.</param>
        /// <exception cref="ArgumentNullException"></exception>
        public PlayerShipManager(Canvas theBackground)
        {
            if (theBackground == null)
            {
                throw new ArgumentNullException(nameof(theBackground));
            }
            this.background = theBackground;
            this.CreateAndPlacePlayerShip();
        }

        /// <summary>
        ///     Moves the player ship to the left.
        ///     Precondition: none
        ///     Postcondition: The player ship has moved left.
        /// </summary>
        public void MovePlayerShipLeft()
        {
            const int applicationBeginningWidth = 0;
            if (this.PlayerShip.X <= applicationBeginningWidth)
            {
                this.PlayerShip.MoveRight();
            }
            this.PlayerShip.MoveLeft();
        }

        /// <summary>
        ///     Moves the player ship to the right.
        ///     Precondition: none
        ///     Postcondition: The player ship has moved right.
        /// </summary>
        public void MovePlayerShipRight()
        {
            if (this.PlayerShip.X >= (this.background.Width - this.PlayerShip.Width))
            {
                this.PlayerShip.MoveLeft();
            }
            this.PlayerShip.MoveRight();
        }

        /// <summary>
        ///     Creates and places the player ship.
        ///     Precondition: None
        ///     Postcondition: The player ship has been created and placed
        /// </summary>
        public void CreateAndPlacePlayerShip()
        {
            this.PlayerShip = ShipFactory.CreatePlayerShip();
            this.background.Children.Add(this.PlayerShip.Sprite);

            this.placePlayerShipNearBottomOfBackgroundCentered();
        }

        private void placePlayerShipNearBottomOfBackgroundCentered()
        {
            this.PlayerShip.X = this.background.Width / 2.0 - this.PlayerShip.Width / 2.0;
            this.PlayerShip.Y = this.background.Height - this.PlayerShip.Height - PlayerShipBottomOffset;
        }
    }
}